from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import VoteSerializer, CommentSerializer, ConversationSerializer
from .models import Vote, Comment, Conversation

from django.contrib.auth import get_user_model

User = get_user_model()


class VoteView(APIView):

	def get(self, request):
		votes = Vote.objects.all()
		serializer = VoteSerializer(votes, many=True)
		return Response(serializer.data)


class CommentView(APIView):

	def get(self, request):
		comments = Comment.objects.all()
		serializer = CommentSerializer(comments, many=True)
		return Response(serializer.data)


class ConversationView(APIView):

	def get(self, request):
		conversations = Conversation.objects.all()
		serializer = ConversationSerializer(conversations, many=True)
		return Response(serializer.data)

	def post(self, request, format=None):
		serializer = ConversationSerializer(data=request.data)
		if serializer.is_valid():
		    serializer.save()
		    return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)