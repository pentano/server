from conversations.models import Conversation, Vote, Participant, Comment


def increase_participation_from_comment(sender, instance, created, **kwargs):
    conversation = instance.conversation

    participant, is_new_participant = Participant.objects.get_or_create(
        conversation_id=conversation.id,
        user_id=instance.author_id,
        defaults={
            'conversation_id': conversation.id,
            'user_id': instance.author_id,
            'p_index': conversation.n_participants,
        }
    )

    if(is_new_participant):
        conversation.n_participants += 1

    if(created):
        instance.c_index = conversation.n_comments
        conversation.n_comments += 1

    conversation.save()


def increase_participation_from_vote(sender, instance, **kwargs):
    conversation = instance.conversation

    participant, is_new_participant = Participant.objects.get_or_create(
        conversation_id=conversation.id,
        user_id=instance.author_id,
        defaults={
            'conversation_id': conversation.id,
            'user_id': instance.author_id,
            'p_index': conversation.n_participants,
        }
    )

    if(is_new_participant):
        conversation.n_participants += 1
        conversation.save()

    instance.c_index = instance.comment.c_index
    instance.p_index = participant.p_index
