from clusters.models import ClusterManager

def compute_clusters_for(sender, instance, created, **kwargs):
    cm = ClusterManager(instance)
    cm.compute_clusters()
