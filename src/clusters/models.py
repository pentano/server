from django.db import models
from jsonfield import JSONField
from django.conf import settings
from conversations.models import Vote, Conversation
from sklearn.metrics import silhouette_score

from collections import Counter
from sklearn.decomposition import PCA
import numpy as np
from sklearn.cluster import KMeans

import logging
logger = logging.getLogger("project")


class ClusterData(models.Model):
    conversation = models.ForeignKey(
        'conversations.Conversation',
        related_name='clusters',
    )

    n_clusters = models.IntegerField(null=True, blank=True)
    centroids = JSONField(null=True, blank=True)
    silhouette = models.FloatField(null=True, blank=True)
    labels = JSONField(null=True, blank=True)
    members = JSONField(null=True, blank=True)

    def __str__(self):
        return """
            Conversation id: %s,\n
            N_clusters: %s\n,
            Labels: %s,
            """ % (self.conversation_id, self.n_clusters, self.labels)


class ClusterManager():
    def __init__(self, vote, *args, **kwargs):
        self.vote = vote
        self.conversation = vote.conversation
        self.k_max = min(5, self.conversation.n_participants)
        self.n_components = 2

    def ready_to_math(self):
        return self.conversation.n_participants > 3

    def get_reduced_dataset(self):
        data_matrix = ConversationDataMatrix(self.conversation)

        if(self.conversation.n_comments > self.n_components):
            data_matrix.apply_pca(self.n_components)

        return data_matrix.dataset

    def compute_clusters(self):

        best_silhouette = -1
        best_kmeans = None
        dataset = self.get_reduced_dataset()

        for k in range(0, self.k_max):
            #number of clusters (k) goes from 1 to 5
            kmeans = compute_kmeans(dataset, k + 1)
            if(kmeans.silhouette > best_silhouette):
                best_silhouette = kmeans.silhouette
                best_kmeans = kmeans

        return self.save_clusters(best_silhouette, best_kmeans, dataset)

    def save_clusters(self, silhouette, kmeans, members):
       cluster_data, created = ClusterData.objects.get_or_create(
            conversation_id = self.conversation.id,
            silhouette = silhouette,
            centroids = kmeans.cluster_centers_,
            n_clusters = kmeans.n_clusters,
            labels = kmeans.labels_,
            members = members
       )
       return cluster_data


    def complete_dataset(self):
        data_matrix = ConversationDataMatrix(self.conversation)
        return data_matrix.dataset


class ConversationDataMatrix():
    def __init__(self, conversation):
        self.conversation = conversation
        self.rank  = [conversation.n_participants, conversation.n_comments]
        self.dataset = self.build_dataset()

    def apply_pca(self, n_components):
        decomposition = PCA(n_components=n_components)
        try:
            self.dataset = decomposition.fit_transform(self.dataset)
        except:
            return self.dataset

    def load_votes(self):
        if not(self.conversation):
            return None

        return Vote.objects.filter(conversation_id=self.conversation.id).values('p_index', 'c_index', 'value').order_by()

    def build_dataset(self):
        if not(self.conversation):
            logger.warn('Failed building dataset for empty conversation')
            return None

        dataset = np.zeros(self.rank)

        for vote in self.load_votes():
          p_index = vote['p_index']
          c_index = vote['c_index']
          value = vote['value']
          dataset[p_index][c_index] = value

        return dataset


def compute_kmeans(dataset, k):
    logger.warn("\n\nPrepare to calculate clusters for k = %s" % k)
    kmeans = KMeans(n_clusters=k, max_iter=300, verbose=0, random_state=0)
    kmeans.silhouette = 0
    kmeans.cluster_centers_ = []
    kmeans.labels_ = []

    try:
        kmeans.fit_predict(dataset)
        logger.warn("Successfull kmeans calculation for k = %s" % k)
        kmeans.silhouette = compute_silhouette(dataset, kmeans.labels_, k)

    except:
        logger.warn("Could not calculate clusters for k = %s" % k)

    return kmeans


def compute_silhouette(dataset, labels, k):
    silhouette = 0
    try:
        silhouette = silhouette_score(dataset, labels)
        logger.warn("Successfull silhouette calculation for k = %s" % k)

    except:
        logger.warn("Could not calculate silhouette for k = %s" % k)

    return silhouette
