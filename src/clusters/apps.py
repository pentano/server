from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

from django.db.models.signals import post_save
from django.conf import settings

class ClustersConfig(AppConfig):
    name = 'clusters'
    verbose_name = _('clusters')

    def ready(self):
        from clusters.signals import compute_clusters_for
        post_save.connect(
            compute_clusters_for,
            sender='conversations.Vote',
            dispatch_uid=settings.SIGNALS["compute_clusters_for"]["uid"]
        )
