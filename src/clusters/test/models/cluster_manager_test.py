from clusters.models import *
from model_mommy import mommy
from unittest.mock import MagicMock
from numpy.testing import assert_array_equal
from numpy.testing import assert_array_almost_equal

from collections import Counter
import numpy as np

from clusters.test.test_helper import *

from pytest_mock import mocker
import mock

import csv
import re
from os.path import dirname, join, exists

@pytest.fixture()
def setup():
    conversation = mommy.make(Conversation)
    user = mommy.make(settings.AUTH_USER_MODEL)

    comment = mommy.make(
        'conversations.Comment',
        conversation=conversation,
        author=user,
        c_index=0,
    )

    vote = mommy.make(
        'conversations.Vote',
        comment=comment,
        author=user,
        value=1,
        conversation=conversation,
        c_index=0,
        p_index=0,
    )

    cm = ClusterManager(vote)
    conversation.n_participants = 1
    conversation.n_comments = 1
    cm.k_max = min(5, conversation.n_participants)
    return (cm)

@pytest.mark.django_db
def test_ready_to_math_should_return_false_for_n_participants_below_4(setup):
    cm = setup
    cm.vote.conversation.n_participants = 3
    assert not cm.ready_to_math()

    cm.vote.conversation.n_participants = 2
    assert not cm.ready_to_math()

    cm.vote.conversation.n_participants = -2
    assert not cm.ready_to_math()


@pytest.mark.django_db
def test_ready_to_math_should_return_true_for_n_participants_above_3(setup):
    cm = setup
    cm.vote.conversation.n_participants = 4
    assert cm.ready_to_math()

    cm.vote.conversation.n_participants = 5
    assert cm.ready_to_math()


@pytest.mark.django_db
def test_should_get_dataset_for_1_vote_1_participant_1_comment(setup):
    cm = setup
    cm.conversation.n_participants = 1
    cm.conversation.n_comments = 1

    assert [1] == cm.get_reduced_dataset()


@pytest.mark.django_db
def test_should_get_complete_dataset_6_participants_in_2_comments(setup):
    cm = setup
    conv = cm.conversation
    author = cm.vote.author

    # signals are turned off for faster execution,
    # so we need to set up conversation parameters manually
    cm.conversation.n_participants = 6
    cm.conversation.n_comments = 2

    votes_data = [[ 1,  0,],
                  [ 1,  0,],
                  [ 1, -1,],
                  [ 0, -1,],
                  [-1,  1,],
                  [-1,  0,]]

    comments = mommy.make(
        'conversations.Comment',
        1,
        conversation=conv,
        author=author,
    )

    users = mommy.make(settings.AUTH_USER_MODEL, 5)

    # inserting the user created on setup and its votes to the lists of
    # users and votes. Inserting in the beginning to maintain coherence
    users.insert(0, author)
    comments.insert(0, cm.vote.comment)

    for index, votes in enumerate(votes_data):
        [mommy.make(
            'conversations.Vote',
            comment=comments[i],
            author=users[index],
            value=value,
            conversation=conv,
            c_index=i,
            p_index=index
        )
        for i,value in enumerate(votes)]

    assert_array_equal(votes_data, cm.complete_dataset())


@pytest.mark.django_db
def test_should_get_reduced_dataset_6_participants_in_2_comments(setup):
    cm = setup
    conv = cm.conversation
    author = cm.vote.author

    # signals are turned off for faster execution,
    # so we need to set up conversation parameters manually
    cm.conversation.n_participants = 6
    cm.conversation.n_comments = 2

    votes_data = [[ 1,  0,],
                  [ 1,  0,],
                  [ 1, -1,],
                  [ 0, -1,],
                  [-1,  1,],
                  [-1,  0,]]

    comments = mommy.make(
        'conversations.Comment',
        1,
        conversation=conv,
        author=author,
    )

    users = mommy.make(settings.AUTH_USER_MODEL, 5)

    # inserting the user created on setup and its votes to the lists of
    # users and votes. Inserting in the beginning to maintain coherence
    users.insert(0, author)
    comments.insert(0, cm.vote.comment)

    for index, votes in enumerate(votes_data):
        [mommy.make(
            'conversations.Vote',
            comment=comments[i],
            author=users[index],
            value=value,
            conversation=conv,
            c_index=i,
            p_index=index
        )
        for i,value in enumerate(votes)]

    assert_array_equal(votes_data, cm.get_reduced_dataset())


@pytest.mark.django_db
def test_should_get_complete_dataset_6_participants_in_5_comments(setup):
    cm = setup
    conv = cm.conversation
    author = cm.vote.author

    # signals are turned off for faster execution,
    # so we need to set up conversation parameters manually
    cm.conversation.n_participants = 6
    cm.conversation.n_comments = 5

    votes_data = [[ 1,  0,  0,  0,  0],
                  [ 1,  0,  0, -1,  0],
                  [ 1, -1, -1, -1, -1],
                  [ 0, -1,  1,  0,  0],
                  [-1,  1,  1,  1, -1],
                  [-1,  0,  1,  1,  1]]

    comments = mommy.make(
        'conversations.Comment',
        4,
        conversation=conv,
        author=author,
    )

    users = mommy.make(settings.AUTH_USER_MODEL, 5)

    # inserting the user created on setup and its votes to the lists of
    # users and votes. Inserting in the beginning to maintain coherence
    users.insert(0, author)
    comments.insert(0, cm.vote.comment)

    for index, votes in enumerate(votes_data):
        [mommy.make(
            'conversations.Vote',
            comment=comments[i],
            author=users[index],
            value=value,
            conversation=conv,
            c_index=i,
            p_index=index
        )
        for i,value in enumerate(votes)]

    assert_array_equal(votes_data, cm.complete_dataset())


@pytest.mark.django_db
def test_should_get_reduced_dataset_6_participants_in_5_comments(setup):
    cm = setup
    conv = cm.conversation
    author = cm.vote.author

    # signals are turned off for faster execution,
    # so we need to set up conversation parameters manually

    votes_data = [[ 1,  0,  0,  0,  0],
                  [ 1,  0,  0, -1,  0],
                  [ 1, -1, -1, -1, -1],
                  [ 0, -1,  1,  0,  0],
                  [-1,  1,  1,  1, -1],
                  [-1,  0,  1,  1,  1]]

    users = mommy.make(settings.AUTH_USER_MODEL, 5)
    comments = mommy.make(
        'conversations.Comment',
        4,
        conversation=conv,
        author=author,
    )

    conv.n_comments += len(comments)
    conv.n_participants += len(users)

    # inserting the user created on setup and its votes to the lists of
    # users and votes. Inserting in the beginning to maintain coherence
    users.insert(0, author)
    comments.insert(0, cm.vote.comment)

    for index, votes in enumerate(votes_data):
        [mommy.make(
            'conversations.Vote',
            comment=comments[i],
            author=users[index],
            value=value,
            conversation=conv,
            c_index=i,
            p_index=index
        )
        for i,value in enumerate(votes)]

    # calculated from sklearn PCA
    reduced_data=[[ 0.57755895, -0.0492889 ],
                  [ 1.12736995, -0.11121576],
                  [ 2.06872482,  0.3854312 ],
                  [-0.19851852, -0.69134937],
                  [-1.76313314,  1.31803543],
                  [-1.81200206, -0.85161258]]

    assert_array_almost_equal(reduced_data, cm.get_reduced_dataset(), 8)


@pytest.mark.django_db
def test_compute_clusters_should_save_cluster_data(setup):
    cm = setup

    old_count = ClusterData.objects.count()
    cluster_data = cm.compute_clusters()

    assert old_count + 1 == ClusterData.objects.count()
    assert type(cluster_data) == ClusterData

