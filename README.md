# Pentano

![build](https://gitlab.com/pentano/server/badges/master/build.svg)
![coverage](https://gitlab.com/pentano/server/badges/master/coverage.svg)

Pentano is a platform for online conversations. It is built with [Python][0]
using the [Django Web Framework][1].

This project has the following basic apps:

* Conversations (short desc)
* Clusters (short desc)

## Installation

### Quick start

To set up a development environment quickly, first install Python 3. It
comes with virtualenv built-in. So create a virtual env by:

    1. `$ python3 -m venv pentano`
    2. `$ . pentano/bin/activate`

Install all dependencies:

    pip install -r requirements.txt

Run migrations:

    python manage.py migrate

### Detailed instructions

Take a look at the docs for more information.

[0]: https://www.python.org/
[1]: https://www.djangoproject.com/
